#!/usr/bin/env bash
apt-get update
apt-get install -y apache2
systemctl enable apache2
apt-get install -y php libapache2-mod-php php-mbstring php-xmlrpc php-soap php-gd php-xml php-cli php-zip php-mysql php-curl
systemctl restart apache2
/srv/install.composer.sh
curl -sS https://get.symfony.com/cli/installer | bash
mv /root/.symfony/bin/symfony /usr/local/bin/symfony
cd /home/vagrant
su vagrant -c "symfony new --full vendor"
vendor/bin/phpunit --help
